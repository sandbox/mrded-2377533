<?php

/**
 * This class renders wiki links in XHTML.
 *
 * @category   Text
 * @package    Text_Wiki
 * @author     Paul M. Jones <pmjones@php.net>
 * @license    http://www.gnu.org/copyleft/lesser.html  LGPL License 2.1
 * @version    Release: @package_version@
 * @link       http://pear.php.net/package/Text_Wiki
 */
class Text_Wiki_Render_Xhtml_Wikilink extends Text_Wiki_Render {

  var $conf = array(
    'pages' => array(), // set to null or false to turn off page checks
    'view_url' => 'http://example.com/index.php?page=%s',
    'new_url' => 'http://example.com/new.php?page=%s',
    'new_text' => '?',
    'new_text_pos' => 'after', // 'before', 'after', or null/false
    'css' => null,
    'css_new' => null,
    'style_new' => null,
    'exists_callback' => null, // call_user_func() callback
    'transform_callback' => null,
    'space_replacement' => '',
    'use_wikitools' => FALSE,
    'use_freelinking' => FALSE,
    'use_liquid' => FALSE,
    'wikilink_base' => 'wiki/',
  );

  /**
   * Returns drupal path for a page title.
   *
   * @param string $page
   *
   * @return string
   */
  function pagePath($page) {
    if ($space_replacement = $this->getConf('space_replacement')) {
      $page = str_replace(' ', $space_replacement, $page);
    }

    $path = '';

    // When wikitools is enabled, just create a wikitools link.
    if ($this->getConf('use_wikitools') && module_exists('wikitools')) {
      $path = wikitools_wikilink_drupal_path($page);
    }
    elseif ($this->getConf('use_freelinking')) {
      // When freelinking is enabled, just create a freelinking link.
      $path = 'freelinking/' . urlencode($page);
    }
    elseif ($this->getConf('use_liquid')) {
      // When liquid is enabled, create a link to liquid.
      $path = 'wiki/' . urlencode($page);
    }
    else {
      // Try to find the node and link to it directly.
      $nid = db_select('node', 'n')
        ->fields('n', array('nid'))
        ->where('LOWER(title) = LOWER(:page)', array(':page' => $page))
        ->execute()
        ->fetchField();
      if ($nid) {
        $path = 'node/' . $nid;
      }
      else {
        // The page was not found.
        $path = $this->getConf('wikilink_base') . urlencode($page);
      }
    }

    return (variable_get('clean_url', 0) ? '' : '?q=') . $path;
  }

  /**
   *
   * Renders a token into XHTML.
   *
   * @access public
   *
   * @param array $options The "options" portion of the token (second
   * element).
   *
   * @return string The text rendered from the token options.
   *
   */
  function token($options) {
    // Make nice variable names (page, anchor, text).
    $anchor = $options['anchor'];
    $page = $options['page'];
    $text = $options['text'];

    $page_link_name = $this->pagePath($page);
    if (isset($this->conf['transform_callback']) && function_exists($this->conf['transform_callback'])) {
      $page_link_name = call_user_func($this->conf['transform_callback'], $page_link_name);
    }

    // Is there a "page existence" callback?
    // We need to access it directly instead of through getConf() because we'll need a reference
    // For object instance method callbacks.
    if (isset($this->conf['exists_callback'])) {
      $callback = & $this->conf['exists_callback'];
    } else {
      $callback = FALSE;
    }

    if ($callback) {
      // Use the callback function.
      $link = call_user_func($callback, $page_link_name);
      $exists = (bool) $link;
    } else {
      // No callback, go to the naive page array.
      $list = $this->getConf('pages');
      $link = $page_link_name;

      $exists = is_array($list)
        ? in_array($page_link_name, $list) // Yes, check against the page list.
        : TRUE; // No, assume it exists.
    }

    $anchor = $anchor ? '#' . $this->urlEncode(substr($anchor, 1)) : '';

    // Does the page exist?
    if ($exists) {
      // PAGE EXISTS.
      // Link to the page view, but we have to build the HREF.
      // We support both the old form where the page always comes at the end,
      // and the new form that uses %s for sprintf()
      $href = $this->getConf('view_url');
      $href = (strpos($href, '%s') === false)
        ? $href . $link . $anchor          // Use the old form (page-at-end).
        : sprintf($href, $link) . $anchor; // Use the new form (sprintf format string).

      // Get the CSS class and generate output.
      $css = sprintf(' class="%s"', $this->textEncode($this->getConf('css')));

      return "<a $css href='{$this->textEncode($href)}'>{$this->textEncode($text)}</a>";
    }

    else {
      // PAGE DOES NOT EXIST.
      // Link to a create-page url, but only if new_url is set.
      $href = $this->getConf('new_url', null);

      // Set the proper HREF
      if ($href) {
        // Yes, link to the new-page href, but we have to build it.
        // We support both the old form where the page always comes at the end,
        // and the new form that uses sprintf().
        $href = (strpos($href, '%s') === false)
          ? $href . $link           // Use the old form.
          : sprintf($href, $link);  // Use the new form.
      }

      // Get the appropriate CSS class and new-link text.
      if (substr($href, 1, 4) == "wiki") {
        $title = str_replace("_", " ", substr(urldecode($href), 6));

        $query = new EntityFieldQuery();
        $query->entityCondition('entity_type', 'node');
        $query->entityCondition('bundle', array('book', 'page'));
        $query->propertyCondition('title', $title);

        $nodes = $query->execute();
        $node = NULL;

        if (!empty($nodes['node'])) {
          $node = reset($nodes['node']);
        } else {
          $css = " class='makenew'";
        }
      }

      $new = $this->getConf('new_text');

      // What kind of linking are we doing?
      $pos = $this->getConf('new_text_pos');

      if (!$pos || !$new) {
        // No position (or no new_text), use css only on the page name
        if (isset($node) && $node->nid) {
          $href = '/' . drupal_get_path_alias("node/" . $node->nid);
        }

        return "<a $css href='{$this->textEncode($href)}'>{$this->textEncode($text)}</a>";
      }
      elseif ($pos == 'before') {
        // Use the new_text BEFORE the page name.
        return "<a $css href='{$this->textEncode($href)}'>{$this->textEncode($new)}</a> " . $this->textEncode($text);
      }
      else {
        // Default, use the new_text link AFTER the page name.
        return $this->textEncode($text) . " <a $css href='{$this->textEncode($href)}'>{$this->textEncode($new)}</a>";
      }
    }
  }
}
